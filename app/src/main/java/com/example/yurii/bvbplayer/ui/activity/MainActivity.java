package com.example.yurii.bvbplayer.ui.activity;

import android.view.Window;

import com.example.yurii.bvbplayer.R;
import com.example.yurii.bvbplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bvbplayer.ui.navigator.NavigatorManager;
import com.example.yurii.bvbplayer.ui.navigator.core.ResourceManager;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Bean
    public NavigatorManager navigatorManager;

    @AfterViews
    public void initView(){
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_TRACK_FRAGMENT);
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.PLAYER_CONTROL_FRAGMENT);
    }
}
