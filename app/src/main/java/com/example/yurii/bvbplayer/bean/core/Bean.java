package com.example.yurii.bvbplayer.bean.core;



import com.example.yurii.bvbplayer.api.core.BaseView;
import com.example.yurii.bvbplayer.ui.activity.core.BaseActivity;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public abstract class Bean<T> implements BaseView<T> {
    @RootContext
    public BaseActivity baseActivity;


    @Override
    public void onFailed(String error) {

    }

    @Override
    public void onSuccess(T t) {

    }

    @Override
    public void showWait() {

    }

    @Override
    public void closeWait() {

    }
}
