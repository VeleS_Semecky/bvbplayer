package com.example.yurii.bvbplayer.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.example.yurii.bvbplayer.R;
import com.example.yurii.bvbplayer.bean.UserBeanPresenter;
import com.example.yurii.bvbplayer.mogel.Track;
import com.example.yurii.bvbplayer.service.PlayerService;
import com.example.yurii.bvbplayer.service.ServiceViewModel;
import com.example.yurii.bvbplayer.ui.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment(R.layout.fragment_player)
public class FirstFragment extends BaseFragment {
    protected Disposable subscription;

    @Bean
    UserBeanPresenter userBeanPresenter;

//    @ViewById(R.id.play)
//    FloatingActionButton playButton;

    @ViewById(R.id.timerValue)
    TextView txtMaxTime;

    @ViewById(R.id.stop)
    FloatingActionButton stopButton;

    @ViewById(R.id.skip_to_next)
    FloatingActionButton skipToNextButton;

    @ViewById(R.id.skip_to_previous)
    FloatingActionButton skipToPreviousButton;

    protected ServiceViewModel mServiceViewModel;

    @AfterViews
    protected void initSVM() {
        mServiceViewModel = ViewModelProviders.of(getBaseActivity()).get(ServiceViewModel.class);
        mServiceViewModel.getMediaMetadata().observe(getBaseActivity(), mediaMetadataCompat -> {
            assert mediaMetadataCompat != null;
//            setLength(Track.getDuration(mediaMetadataCompat));
        });
        mServiceViewModel.getStatePlayback().observe(getBaseActivity(), playbackStateCompat -> {
            if(playbackStateCompat!=null) {
                txtMaxTime.setText(Track.getCorrectDuration((int) playbackStateCompat.getPosition()));
            }
        });
        thTimer();
    }


    @Click(R.id.play)
    protected void playButton(FloatingActionButton fabPlay) {
        if(mServiceViewModel.getMyPlayerState() == PlaybackStateCompat.STATE_PLAYING) {
            fabPlay.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            mServiceViewModel.pauseMusic();
        }else {
            fabPlay.setImageResource(R.drawable.ic_pause_black_24dp);
            mServiceViewModel.playMusic();
            thTimer();
        }

    }

//    @Click(R.id.pause)
//    protected void pauseButton() {
//        if (mediaController != null)
//            mediaController.getTransportControls().pause();
//    }

    @Click(R.id.stop)
    protected void stopButton() {
        mServiceViewModel.stopMusic();

    }

    @Click(R.id.skip_to_next)
    protected void skipToNextButton() {
        mServiceViewModel.toNextMusic();

    }

    @Click(R.id.skip_to_previous)
    protected void skipToPreviousButton() {
        mServiceViewModel.toPreyMusic();

    }


    private void setLength(long length) {
        txtMaxTime.setText(Track.getCorrectDuration(length));
    }

    private void thTimer(){
        subscription = Flowable.timer(1, TimeUnit.SECONDS)
                .repeat()
                .flatMapCompletable(i -> doStuffAsync())
                .subscribe();
    }

    private Completable doStuffAsync() {
        return Completable
                .fromRunnable(() -> {
                    txtMaxTime.setText(Track.getCorrectDuration((int) mServiceViewModel.getPosition()));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> Log.e("getPosition","Stuff failed", e))
                .onErrorComplete();
    }
    @Override
    public void onDetach() {
        subscription.dispose();
        super.onDetach();
    }

}
