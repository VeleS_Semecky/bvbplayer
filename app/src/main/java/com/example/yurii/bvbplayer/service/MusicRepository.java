package com.example.yurii.bvbplayer.service;

import android.net.Uri;

import com.example.yurii.bvbplayer.R;


final class MusicRepository {

    private final Track[] data = {
            new Track("Triangle", "Jason Shaw", R.drawable.image266680, Uri.parse("http://cdndl.zaycev.net/97258/6756678/system_of_a_down_-_toxicity_%28zaycev.net%29.mp3"), 0),
            new Track("Rubix Cube", "Jason Shaw", R.drawable.image396168, Uri.parse("http://cdndl.zaycev.net/13822/1637230/bon_jovi_-_it_s_my_life_%28zaycev.net%29.mp3"), 0),
            new Track("MC Ballad S Early Eighties", "Frank Nora", R.drawable.image396168, Uri.parse("http://cdndl.zaycev.net/76134/3135835/nirvana_-_rape_me_%28zaycev.net%29.mp33"), (2 * 60 + 50) * 1000),
            new Track("Folk Song", "Brian Boyko", R.drawable.image396168, Uri.parse("http://cdndl.zaycev.net/2619/2999578/ac_dc_-_1979_-_highway_to_hell_%28zaycev.net%29.mp3"), (3 * 60 + 5) * 1000),
    };

    private final int maxIndex = data.length - 1;
    private int currentItemIndex = 0;

    Track getNext() {
        if (currentItemIndex == maxIndex)
            currentItemIndex = 0;
        else
            currentItemIndex++;
        return getCurrent();
    }

    Track getPrevious() {
        if (currentItemIndex == 0)
            currentItemIndex = maxIndex;
        else
            currentItemIndex--;
        return getCurrent();
    }

    Track getCurrent() {
        return data[currentItemIndex];
    }

    static class Track {

        private String title;
        private String artist;
        private int bitmapResId;
        private Uri uri;
        private long duration; // in ms

        Track(String title, String artist, int bitmapResId, Uri uri, long duration) {
            this.title = title;
            this.artist = artist;
            this.bitmapResId = bitmapResId;
            this.uri = uri;
            this.duration = duration;
        }

        String getTitle() {
            return title;
        }

        String getArtist() {
            return artist;
        }

        int getBitmapResId() {
            return bitmapResId;
        }

        Uri getUri() {
            return uri;
        }

        long getDuration() {
            return duration;
        }
    }
}
