package com.example.yurii.bvbplayer.bean;

import android.widget.Toast;


import com.example.yurii.bvbplayer.api.model.User;
import com.example.yurii.bvbplayer.api.presenter.UserPresenter;
import com.example.yurii.bvbplayer.bean.core.UserBean;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;

@EBean
public class UserBeanPresenter extends UserBean {
    @Override
    public void init() {
        super.init();
        getUserDeps().inject(this);
    }

    @AfterViews
    public void getUser(){
        new UserPresenter(networkUser,this).getUser("Yura");
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(baseActivity, user.getName(), Toast.LENGTH_SHORT).show();
    }
}
