package com.example.yurii.bvbplayer.service;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.util.SparseArray;

import com.example.yurii.bvbplayer.R;
import com.example.yurii.bvbplayer.mogel.Track;
import com.example.yurii.bvbplayer.mogel.TrackRepository;
import com.example.yurii.bvbplayer.service.core.BvbPlayerService;

import com.example.yurii.bvbplayer.service.core.CreateAudioFocus;
import com.example.yurii.bvbplayer.service.core.NotificationAndForegroundStatus;
import com.example.yurii.bvbplayer.ui.activity.MainActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.List;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import okhttp3.OkHttpClient;


public class BvbMediaSession extends MediaSessionCompat.Callback implements AudioManager.OnAudioFocusChangeListener,Player.EventListener,LifecycleObserver {

    public BvbMediaSession(BvbPlayerService baseMusicService) {
        this.baseMusicService = baseMusicService;
    }
    //
    ExtractorMediaSource.Factory extractorMediaSource;
    private List<Track> thListTrack;
    private Track thTrack;
    private int idMyTrack = 0;

    private AudioManager audioManager;

    private BvbPlayerService baseMusicService;

    private TrackRepository thTrackRepository;

    private boolean audioFocusRequested = false;
    private AudioFocusRequest audioFocusRequest;

    private int currentState = PlaybackStateCompat.STATE_STOPPED;


    private MediaSessionCompat mediaSession;


    public MediaSessionCompat getMediaSession() {
        return mediaSession;
    }

    private SimpleExoPlayer exoPlayer;

    private final PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder().setActions(
                      PlaybackStateCompat.ACTION_PLAY
                    | PlaybackStateCompat.ACTION_STOP
                    | PlaybackStateCompat.ACTION_PAUSE
                    | PlaybackStateCompat.ACTION_PLAY_PAUSE
                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    );

    private ExtractorsFactory extractorsFactory;
    private DataSource.Factory dataSourceFactory;

    @Override
    public void onPlayFromSearch(String query, Bundle extras) {
        if (!exoPlayer.getPlayWhenReady()) {
            baseMusicService.startService(new Intent(baseMusicService, BvbPlayerService.class));

//            setTrackInExoPlayer(query);
            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(CreateAudioFocus.getAudioFocusChangeListener(this), AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            baseMusicService.registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            exoPlayer.setPlayWhenReady(true);

        }
        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, exoPlayer.getCurrentPosition(), 1).build());
        currentState = PlaybackStateCompat.STATE_PLAYING;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());
    }

    @Override
    public void onPlay() {
        exoPlayer.setPlayWhenReady(false);
        if (!exoPlayer.getPlayWhenReady()) {
            baseMusicService.startService(new Intent(baseMusicService, BvbPlayerService.class));
            //OR
            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(CreateAudioFocus.getAudioFocusChangeListener(this), AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            baseMusicService.registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
//
            exoPlayer.setPlayWhenReady(true);
            currentState = PlaybackStateCompat.STATE_PLAYING;
            setTrackInPlayer(idMyTrack);
        }


    }

    @Override
    public void onStop() {
        if (exoPlayer.getPlayWhenReady()) {
            exoPlayer.setPlayWhenReady(false);
            baseMusicService.unregisterReceiver(becomingNoisyReceiver);
        }

        if (audioFocusRequested) {
            audioFocusRequested = false;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                audioManager.abandonAudioFocusRequest(audioFocusRequest);
            } else {
                audioManager.abandonAudioFocus(CreateAudioFocus.getAudioFocusChangeListener(this));
            }
        }

        mediaSession.setActive(false);

        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
        currentState = PlaybackStateCompat.STATE_STOPPED;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());


        baseMusicService.stopSelf();
    }



    @Override
    public void onPause() {
        // Останавливаем воспроизведение
        if (exoPlayer.getPlayWhenReady()) {
            exoPlayer.setPlayWhenReady(false);
            baseMusicService.unregisterReceiver(becomingNoisyReceiver);
        }
        // Сообщаем новое состояние
        mediaSession.setPlaybackState(
                stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());

        currentState = PlaybackStateCompat.STATE_PAUSED;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());

    }

    @Override
    public void onSkipToNext() {
        idMyTrack++;
        Log.e("onSkipToNext", String.valueOf(idMyTrack));
        if(idMyTrack<thListTrack.size()) {
            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.setPlayWhenReady(false);
                baseMusicService.unregisterReceiver(becomingNoisyReceiver);
            }
            setTrackInPlayer(idMyTrack);
//            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, exoPlayer.getCurrentPosition(), 1).build());
//            baseMusicService.registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
//            NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState, baseMusicService, getMediaSession());
        }else {
            this.onStop();
        }
    }

    @Override
    public void onSkipToPrevious() {
        idMyTrack--;
        Log.e("onSkipToPrevious", String.valueOf(idMyTrack));
        if(idMyTrack>=0) {
            setTrackInPlayer(idMyTrack);
            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.setPlayWhenReady(false);
                baseMusicService.unregisterReceiver(becomingNoisyReceiver);
            }
            exoPlayer.setPlayWhenReady(true);
            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, exoPlayer.getCurrentPosition(), 1).build());
            baseMusicService.registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
            NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState, baseMusicService, getMediaSession());
        }else {
            this.onStop();
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playWhenReady && playbackState == Player.STATE_ENDED) {
            this.onSkipToNext();
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
    MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();
    private void setTrackInPlayer(int id){
        idMyTrack = id;
        thTrack = thListTrack.get(id);

        if(!Track.isYouTube(thTrack.getUrl())){
            setUrlTrack(thTrack.getUrl());
        }else if(Track.isYouTube(thTrack.getUrl())){
            getYoutubeDownloadUrl(Track.keyYouTube(thTrack.getUrl()),baseMusicService.getApplicationContext());
        }
    }

    private void setUrlTrack(String url){
//                .createMediaSource(Uri.parse(url));
        exoPlayer.prepare(extractorMediaSource.createMediaSource(Uri.parse(url)));
        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, exoPlayer.getDuration());
        mediaSession.setMetadata(metadataBuilder.build());
        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, exoPlayer.getCurrentPosition(),
                1).build());
        exoPlayer.setPlayWhenReady(true);
        currentState = PlaybackStateCompat.STATE_PLAYING;
        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,mediaSession);

    }


    private void getYoutubeDownloadUrl(String youtubeLink, Context context) {
         @SuppressLint("StaticFieldLeak")
         YouTubeExtractor youTubeExtractor =
                new YouTubeExtractor(context) {
                    @Override
                    public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                        if (ytFiles == null) {
                            // Something went wrong we got no urls. Always check this.
                            return;
                        }

                        // Iterate over iTags
                        for (int i = 0, iTag; i < ytFiles.size(); i++) {
                            iTag = ytFiles.keyAt(i);
                            // ytFile represents one file with its url and meta data
                            YtFile ytFile = ytFiles.get(iTag);

                            // Just add videos in a decent format => height -1 = audio
                            if (ytFile.getFormat().getHeight() == -1 || ytFile.getFormat().getHeight() >= 240) {
                                setUrlTrack(ytFile.getUrl());
                                Log.e("qwsasdzd", vMeta.getTitle() + " " + ytFile.getUrl());
                                return;
                            }
                        }
                    }
                };
        youTubeExtractor.extract(youtubeLink, true, false);
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreateMediaSession(){
        Log.e("ServiceMediaSession","onCreateMediaSession");
        audioFocusRequest = CreateAudioFocus.getAudioFocusRequest(this,baseMusicService);

        audioManager = (AudioManager) baseMusicService.getSystemService(Context.AUDIO_SERVICE);


        mediaSession = new MediaSessionCompat(baseMusicService, " MyMusicService");
        mediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS
                        | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(this);

        Intent activityIntent = new Intent(baseMusicService, MainActivity.class);
        mediaSession.setSessionActivity(
                PendingIntent.getActivity(baseMusicService, 0, activityIntent, 0));

        Intent mediaButtonIntent = new Intent(
                Intent.ACTION_MEDIA_BUTTON, null, baseMusicService, MediaButtonReceiver.class);
        mediaSession.setMediaButtonReceiver(
                PendingIntent.getBroadcast(baseMusicService, 0, mediaButtonIntent, 0));


        DataSource.Factory httpDataSourceFactory = new OkHttpDataSourceFactory(new OkHttpClient(), Util.getUserAgent(baseMusicService, baseMusicService.getString(R.string.app_name)), null);
        Cache cache = new SimpleCache(new File(baseMusicService.getObbDir().getAbsolutePath() + "/exoplayer"), new LeastRecentlyUsedCacheEvictor(1024 * 1024 * 100)); // 100 Mb max
        this.dataSourceFactory = new CacheDataSourceFactory(cache, httpDataSourceFactory, CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
        this.extractorsFactory = new DefaultExtractorsFactory();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(baseMusicService), new DefaultTrackSelector(), new DefaultLoadControl());
        thTrackRepository = new TrackRepository(baseMusicService);
        extractorMediaSource =  new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(extractorsFactory);

        thListTrack = thTrackRepository.getListTrack();
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onDestroyMediaSession(){
        Log.e("ServiceMediaSession","onDestroyMediaSession");
        // Ресурсы освобождать обязательно
//        trackRepository.unsubscribeRxJava();
        if(mediaSession!=null) {
            mediaSession.release();
        }
        if(exoPlayer!=null) {
            exoPlayer.release();
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // Фокус предоставлен.
                // Например, был входящий звонок и фокус у нас отняли.
                // Звонок закончился, фокус выдали опять
                // и мы продолжили воспроизведение.
                this.onPlay();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Фокус отняли, потому что какому-то приложению надо
                // коротко "крякнуть".
                // Например, проиграть звук уведомления или навигатору сказать
                // "Через 50 метров поворот направо".
                // В этой ситуации нам разрешено не останавливать вопроизведение,
                // но надо снизить громкость.
                // Приложение не обязано именно снижать громкость,
                // можно встать на паузу, что мы здесь и делаем.
                this.onPause();
                break;
            default:
                // Фокус совсем отняли.
                this.onPause();
                break;
        }
    }

    private final BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Disconnecting headphones - stop playback
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                onPause();
            }
        }
    };

    @Override
    public void onSeekTo(long pos) {
        super.onSeekTo(pos);
        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, pos, 1).build());

    }

}
