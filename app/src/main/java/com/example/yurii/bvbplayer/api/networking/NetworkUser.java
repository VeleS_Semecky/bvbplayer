package com.example.yurii.bvbplayer.api.networking;


import com.example.yurii.bvbplayer.api.model.User;
import com.example.yurii.bvbplayer.api.model.core.Head;
import com.example.yurii.bvbplayer.api.networking.core.Service;
import com.example.yurii.bvbplayer.api.networking.error.NetworkError;
import com.example.yurii.bvbplayer.api.service.UserApi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class NetworkUser extends Service<UserApi> {

    public NetworkUser(UserApi networkService) {
        super(networkService);
    }

    public Disposable getUser(String userName, Callback<User> headCallback){
        return networkService.getUser(userName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<Head<User>>() {


                    @Override
                    public void onError(Throwable e) {
                        headCallback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onComplete() {
                    
                    }

                    @Override
                    public void onNext(Head<User> userHead) {
                        headCallback.onSuccess(userHead.getT());
                    }
                });
    }


}
