package com.example.yurii.mylikeproject.ui.fragment;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.adapter.bean.ProviderBeanTrack;
import com.example.yurii.mylikeproject.ui.fragment.core.BaseFragment;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_track_start)
public class ListTrackFragment extends BaseFragment {

    @Bean
    ProviderBeanTrack providerBeanTrack;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //    @Click(R.id.fab_add_track)
//    public void addNewShop(){
//        navigatorManager.getMainManager(null).
//                moveFragmentTo(ResourceManager.FragmentId.LIST_TRACK_FRAGMENT);
//    }
}
