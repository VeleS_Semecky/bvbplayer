package com.example.yurii.mylikeproject.ui.activity;

import android.view.Window;

import com.example.yurii.mylikeproject.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends com.example.yurii.mylikeproject.ui.activity.core.BaseActivity {
    @Bean
    public com.example.yurii.mylikeproject.ui.navigator.NavigatorManager navigatorManager;

    @AfterViews
    public void initView(){
        navigatorManager.getMainManager(null).moveFragmentTo(com.example.yurii.mylikeproject.ui.navigator.core.ResourceManager.FragmentId.LIST_TRACK_FRAGMENT);
        navigatorManager.getMainManager(null).moveFragmentTo(com.example.yurii.mylikeproject.ui.navigator.core.ResourceManager.FragmentId.PLAYER_CONTROL_FRAGMENT);
    }
}
