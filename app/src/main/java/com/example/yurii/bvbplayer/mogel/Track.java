package com.example.yurii.bvbplayer.mogel;

import android.support.annotation.NonNull;
import android.support.v4.media.MediaMetadataCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Track {
    public Track(String url) {
        this.url = url;
    }

    private int id;
    private String url;
    private String keyCach;

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getKeyCach() {
        return keyCach;
    }

    public static String keyYouTube(String url){
        return url.replace("https://www.youtube.com/watch?v=","");
    }

    public static boolean isYouTube(String url){
        return url.contains("https://www.youtube.com/watch?v=");
    }

    @NonNull
    public static String getCorrectDuration(long songs_duration) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(songs_duration);
        int minutes = cal.get(Calendar.MINUTE);
        String timeString;
        if (minutes>60) {
            timeString =
                    new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(cal.getTime());
        }else {
            timeString =
                    new SimpleDateFormat("mm:ss",Locale.getDefault()).format(cal.getTime());
        }
        return timeString;
    }
    public static long getDuration(MediaMetadataCompat metadata){
        return metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
    }
}
