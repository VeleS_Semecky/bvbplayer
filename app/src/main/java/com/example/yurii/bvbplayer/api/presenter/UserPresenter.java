package com.example.yurii.bvbplayer.api.presenter;


import com.example.yurii.bvbplayer.api.core.BaseView;
import com.example.yurii.bvbplayer.api.model.User;
import com.example.yurii.bvbplayer.api.networking.NetworkUser;
import com.example.yurii.bvbplayer.api.networking.core.Service;
import com.example.yurii.bvbplayer.api.networking.error.NetworkError;
import com.example.yurii.bvbplayer.api.presenter.core.Presenter;


public class UserPresenter extends Presenter<NetworkUser,User> {
    public UserPresenter(NetworkUser service, BaseView<User> userBaseView) {
        super(service, userBaseView);
    }

    public void getUser(String user){
        yBaseView.showWait();
        subscription.add(service.getUser(user, new Service.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                yBaseView.closeWait();
                yBaseView.onSuccess(user);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
//                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
}
