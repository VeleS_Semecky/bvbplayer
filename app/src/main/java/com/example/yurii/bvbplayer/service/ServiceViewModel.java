package com.example.yurii.bvbplayer.service;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.Toast;


import com.example.yurii.bvbplayer.mogel.Track;
import com.example.yurii.bvbplayer.service.core.BvbPlayerService;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;

public class ServiceViewModel extends AndroidViewModel {
    private BvbPlayerService.MyBinder thBinder;
    private MediaControllerCompat mediaController ;
    private ServiceConnection serviceConnection;
    private MediaControllerCompat.Callback callback;

    private MutableLiveData<MediaMetadataCompat> mediaMetadata = new MutableLiveData<>();
    private MutableLiveData<PlaybackStateCompat> statePlayback = new MutableLiveData<>();

    public MutableLiveData<MediaMetadataCompat> getMediaMetadata() {
        return mediaMetadata;
    }

    public MutableLiveData<PlaybackStateCompat> getStatePlayback() {
        return statePlayback;
    }

    private boolean playing;

    private int myPlayerState;

    public boolean isPlaying() {
        return playing;
    }

    public ServiceViewModel(@NonNull Application application) {
        super(application);
        callback = new MediaControllerCompat.Callback() {
            @Override
            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                statePlayback.postValue(state);
                if (state == null)
                    return;
                playing = state.getState() == PlaybackStateCompat.STATE_PLAYING;
                myPlayerState = state.getState();
            }

            @Override//test
            public void onMetadataChanged(MediaMetadataCompat metadata) {
                super.onMetadataChanged(metadata);
                Log.e("onMetadataChanged", Track.getCorrectDuration(metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                mediaMetadata.postValue(metadata);

            }
        };
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Toast.makeText(getApplication(),"onServiceConnected",Toast.LENGTH_SHORT).show();
                thBinder = (BvbPlayerService.MyBinder) service;
                try {
                    mediaController = new MediaControllerCompat(
                            getApplication(), thBinder.getMediaSessionToken());
                    mediaController.registerCallback(callback);
                    callback.onPlaybackStateChanged(mediaController.getPlaybackState());
//                    Log.e("onServiceConnectedLong",Track.getCorrectDuration(mediaController.getMetadata().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
                } catch (RemoteException e) {
                    e.printStackTrace();
                    mediaController = null;
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

                Toast.makeText(getApplication(),"NOonServiceConnected",Toast.LENGTH_SHORT).show();
                thBinder = null;
                if (mediaController != null) {
                    mediaController.unregisterCallback(callback);
                    mediaController = null;
                }
            }
        };
        application.bindService(new Intent(application, BvbPlayerService.class), serviceConnection, BIND_AUTO_CREATE);
        if(playing){
            Toast.makeText(getApplication(),"playing",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplication(),"NOplaying",Toast.LENGTH_SHORT).show();

        }
    }

    public void playMusic(){
        if (mediaController != null) {
            Log.e("onPlayMusic", "Play");
            mediaController.getTransportControls().play();
        }
        else {
            Log.e("onPlayMusic", "NoPlay");
        }
    }
    public void pauseMusic(){
        if (mediaController != null) {
            Log.e("onPlayMusic", "Play");
            mediaController.getTransportControls().pause();
        }
        else {
            Log.e("onPlayMusic", "NoPlay");
        }
    }
    //Not actual
    public void playMusicFromUri(String uri){
        if (mediaController != null) {
            Log.e("onPlayMusic", "Play");
            Log.e("onPlayMusicUri", uri);
            mediaController.getTransportControls().playFromSearch(uri,null);
        }
        else {
            Log.e("onPlayMusic", "NoPlay");
        }
    }

    public void seekTo(long l){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().seekTo(l);

        }
    }

    public void stopMusic(){

            mediaController.getTransportControls().stop();

//        playerServiceBinder = null;
//        if (mediaController != null) {
////            mediaController.unregisterCallback(callback);
//            mediaController = null;
//        }
//        context.unbindService(serviceConnection);
    }
    

    public long getPosition(){
        if(playing) {
            if (mediaController != null) {
                return mediaController.getPlaybackState().getPosition();
            }
            return 1;
        }
        return 0;
    }


    public void toNextMusic(){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().skipToNext();
        }
    }

    public void toPreyMusic(){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().skipToPrevious();
        }
    }
    public MediaMetadataCompat toLong(){
        if (mediaController != null&&playing&&mediaController.getMetadata()!=null) {
            return mediaController.getMetadata();
        }
        return null;
    }



    @Override
    protected void onCleared() {
        playing = false;
        thBinder = null;
        if (mediaController != null) {
              mediaController.unregisterCallback(callback);
            mediaController = null;
        }

        getApplication().unbindService(serviceConnection);
        Toast.makeText(getApplication(),"Destroy",Toast.LENGTH_SHORT).show();
        super.onCleared();
    }


    public int getMyPlayerState() {
        return myPlayerState;
    }


}
