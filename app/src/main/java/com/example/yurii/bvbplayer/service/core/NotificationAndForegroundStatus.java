package com.example.yurii.bvbplayer.service.core;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.app.NotificationCompat.MediaStyle;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import com.example.yurii.bvbplayer.R;

public class NotificationAndForegroundStatus {
    private final static int NOTIFICATION_ID = 404;

    private final static String NOTIFICATION_DEFAULT_CHANNEL_ID = "MyMusicChannel";

    public static Notification getNotification(int playbackState, Context context,  MediaSessionCompat mediaSession) {
        // MediaStyleHelper заполняет уведомление метаданными трека.
        // Хелпер любезно написал Ian Lake / Android Framework Developer at Google
        // и выложил здесь: https://gist.github.com/ianhanniballake/47617ec3488e0257325c
        NotificationCompat.Builder builder = MediaStyleHelper.from(context, mediaSession);

        // Добавляем кнопки

        // ...на предыдущий трек
        builder.addAction(
                new NotificationCompat.Action(
                        android.R.drawable.ic_media_previous, context.getString(R.string.previous),
                        MediaButtonReceiver.buildMediaButtonPendingIntent(
                                context,
                                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)));

        // ...play/pause
        if (playbackState == PlaybackStateCompat.STATE_PLAYING)
            builder.addAction(
                    new NotificationCompat.Action(
                            android.R.drawable.ic_media_pause, context.getString(R.string.pause),
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                    context,
                                    PlaybackStateCompat.ACTION_PLAY_PAUSE)));
        else
            builder.addAction(
                    new NotificationCompat.Action(
                            android.R.drawable.ic_media_play, context.getString(R.string.play),
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                    context,
                                    PlaybackStateCompat.ACTION_PLAY_PAUSE)));

        // ...на следующий трек
        builder.addAction(
                new NotificationCompat.Action(android.R.drawable.ic_media_next, context.getString(R.string.next),
                        MediaButtonReceiver.buildMediaButtonPendingIntent(
                                context,
                                PlaybackStateCompat.ACTION_SKIP_TO_NEXT)));

        builder.setStyle(new MediaStyle()
                // В компактном варианте показывать Action с данным порядковым номером.
                // В нашем случае это play/pause.
                .setShowActionsInCompactView(1)
                // Отображать крестик в углу уведомления для его закрытия.
                // Это связано с тем, что для API < 21 из-за ошибки во фреймворке
                // пользователь не мог смахнуть уведомление foreground-сервиса
                // даже после вызова stopForeground(false).
                // Так что это костыль.
                // На API >= 21 крестик не отображается, там просто смахиваем уведомление.
                .setShowCancelButton(true)
                // Указываем, что делать при нажатии на крестик или смахивании
                .setCancelButtonIntent(
                        MediaButtonReceiver.buildMediaButtonPendingIntent(
                                context,    
                                PlaybackStateCompat.ACTION_STOP))
                // Передаем токен. Это важно для Android Wear. Если токен не передать,
                // кнопка на Android Wear будет отображаться, но не будет ничего делать
                .setMediaSession(mediaSession.getSessionToken()));

        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

        // Не отображать время создания уведомления. В нашем случае это не имеет смысла
        builder.setShowWhen(false);

        // Это важно. Без этой строчки уведомления не отображаются на Android Wear
        // и криво отображаются на самом телефоне.
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);

        // Не надо каждый раз вываливать уведомление на пользователя
        builder.setOnlyAlertOnce(true);
        //без цієї хернв в Андроїд О не працює
        builder.setChannelId(NOTIFICATION_DEFAULT_CHANNEL_ID);
        return builder.build();
    }

    public static void refreshNotificationAndForegroundStatus(int playbackState, Service baseMusicService, MediaSessionCompat mediaSession) {
        switch (playbackState) {
            case PlaybackStateCompat.STATE_PLAYING: {
                baseMusicService.startForeground(NOTIFICATION_ID,
                        NotificationAndForegroundStatus.getNotification(playbackState,baseMusicService,mediaSession)
                );
                break;
            }
            case PlaybackStateCompat.STATE_PAUSED: {
                // На паузе мы перестаем быть foreground, однако оставляем уведомление,
                // чтобы пользователь мог play нажать
                NotificationManagerCompat.from(baseMusicService)
                        .notify(NOTIFICATION_ID,
                                NotificationAndForegroundStatus.getNotification(playbackState,baseMusicService,mediaSession)
                        );
                baseMusicService.stopForeground(false);
                break;
            }
            case PlaybackStateCompat.STATE_SKIPPING_TO_NEXT: {
                // На паузе мы перестаем быть foreground, однако оставляем уведомление,
                // чтобы пользователь мог play нажать
                NotificationManagerCompat.from(baseMusicService)
                        .notify(NOTIFICATION_ID,
                                NotificationAndForegroundStatus.getNotification(playbackState,baseMusicService,mediaSession)
                        );
                baseMusicService.stopForeground(false);
                break;
            }
            default: {
                // Все, можно прятать уведомление
                baseMusicService.stopForeground(true);
                break;
            }
        }
    }
}
