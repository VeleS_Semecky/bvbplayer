package com.example.yurii.bvbplayer.service.core;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.media.session.PlaybackStateCompat;

import com.example.yurii.bvbplayer.R;
import com.example.yurii.bvbplayer.service.BvbMediaSession;

import javax.inject.Singleton;


public class CreateAudioFocus {
//    @Singleton
    public static AudioFocusRequest getAudioFocusRequest(@NonNull BvbMediaSession bvbMediaSession, Service baseMusicService) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("MyMusicChannel", baseMusicService.getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setSound(null,null);
            NotificationManager notificationManager = (NotificationManager) baseMusicService.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            return new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setOnAudioFocusChangeListener(getAudioFocusChangeListener(bvbMediaSession))
                    .setAcceptsDelayedFocusGain(false)
                    .setWillPauseWhenDucked(true)
                    .setAudioAttributes(audioAttributes)
                    .build();
        }
        return null;
    }
    @Singleton
    public static AudioManager.OnAudioFocusChangeListener getAudioFocusChangeListener(BvbMediaSession bvbMediaSession){
        return focusChange -> {
           switch (focusChange) {
               case AudioManager.AUDIOFOCUS_GAIN:
                   bvbMediaSession.onPlay(); // Не очень красиво
                   break;
               case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                   bvbMediaSession.onPause();
                   break;
               default:
                   bvbMediaSession.onPause();
                   break;
           }
       };
    }

    public static final PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder().setActions(
            PlaybackStateCompat.ACTION_PLAY
                    | PlaybackStateCompat.ACTION_STOP
                    | PlaybackStateCompat.ACTION_PAUSE
                    | PlaybackStateCompat.ACTION_PLAY_PAUSE
                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    );

}
