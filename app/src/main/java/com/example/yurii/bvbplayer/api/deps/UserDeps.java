package com.example.yurii.bvbplayer.api.deps;


import com.example.yurii.bvbplayer.api.networking.module.NetworkModule;
import com.example.yurii.bvbplayer.bean.UserBeanPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface UserDeps  {

    public void inject(UserBeanPresenter userBeanPresenter);


}
