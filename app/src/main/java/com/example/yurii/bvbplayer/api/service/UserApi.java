package com.example.yurii.bvbplayer.api.service;


import com.example.yurii.bvbplayer.api.model.User;
import com.example.yurii.bvbplayer.api.model.core.Head;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface UserApi {
    @GET("get")
    Observable<Head<User>> getUser(@Query("name") String name);
}
