package com.example.yurii.bvbplayer.mogel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.exoplayer2.source.ExtractorMediaSource;

import java.util.ArrayList;
import java.util.List;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

public class TrackRepository{


    public TrackRepository(Context context) {

    }

    private ArrayList<Track> thList;

    public List<Track> getListTrack(){
        ArrayList<Track> thList = new ArrayList<>();
        Track first = new Track("http://dl1-1.mp3party.net/download/1865");
        Track duo = new Track("http://dl2.mp3party.net/download/1263744");
        Track t2 = new Track("https://www.youtube.com/watch?v=HwRL1LNVTLI");
        Track t3 = new Track("https://www.youtube.com/watch?v=Qp6Qn8IwPf8");
        Track t4 = new Track("https://music.я.ws/public/download.php?id=42606859_64463355&hash=ce4384160fd43f39adfb85f47eced1ff7c01d0bd0c76fa2fc319e82f8b1a185d");
        thList.add(t3);
        thList.add(t4);
        thList.add(first);
        thList.add(duo);
        thList.add(t2);
        return thList;
    }


    private void getYoutubeDownloadUrl(String youtubeLink, Context context) {
        @SuppressLint("StaticFieldLeak") YouTubeExtractor youTubeExtractor =
                new YouTubeExtractor(context) {
                    @Override
                    public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                        if (ytFiles == null) {
                            // Something went wrong we got no urls. Always check this.
                            return;
                        }

                        // Iterate over iTags
                        for (int i = 0, iTag; i < ytFiles.size(); i++) {
                            iTag = ytFiles.keyAt(i);
                            // ytFile represents one file with its url and meta data
                            YtFile ytFile = ytFiles.get(iTag);

                            // Just add videos in a decent format => height -1 = audio
                            if (ytFile.getFormat().getHeight() == -1 || ytFile.getFormat().getHeight() >= 240) {

                                Log.e("qwsasdzd", vMeta.getTitle() + " " + ytFile.getUrl());
                                return;
                            }
                        }
                    }
                };
        youTubeExtractor.extract(youtubeLink, true, false);
    }

}
