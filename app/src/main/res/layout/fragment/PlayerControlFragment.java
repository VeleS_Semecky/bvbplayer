package com.example.yurii.mylikeproject.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.model.TrackDataPreferences;
import com.example.yurii.mylikeproject.room.TrackViewModel;
import com.example.yurii.mylikeproject.service.core.MyMediaMetadata;
import com.example.yurii.mylikeproject.service.core.ServiceViewModel;
import com.example.yurii.mylikeproject.ui.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.SeekBarProgressChange;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.yurii.mylikeproject.model.TrackDataPreferences.LONG_TRACK;

@EFragment(R.layout.fragment_player_control)
public class PlayerControlFragment extends BaseFragment {

    protected List<Track> tracks;
    protected ServiceViewModel mServiceViewModel;
    protected Disposable subscription;
    protected TrackViewModel mTrackViewModel;

//    @ViewById(R.id.controlPlay)
//    FloatingActionButton fabPlay;
    @ViewById(R.id.txtMaxTime)
    TextView txtMaxTime;
    @ViewById(R.id.txtNowTime)
    TextView txtNowTime;
    @ViewById(R.id.seekBarMusic)
    SeekBar mSeekBar;
    Timer timer = new Timer();

    @Click(R.id.controlPlay)
    void onPlay(FloatingActionButton fabPlay) {
        if(mServiceViewModel.getMyPlayerState() == PlaybackStateCompat.STATE_PLAYING) {
            fabPlay.setImageResource(R.drawable.ic_pause_black_24dp);
            mServiceViewModel.pauseMusic();
        }else if (mServiceViewModel.getMyPlayerState() == PlaybackStateCompat.STATE_PAUSED){
            fabPlay.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            mServiceViewModel.playMusic();
        }
    }

    @Click(R.id.controlPrey)
    void onPrey() {
       mServiceViewModel.toPreyMusic();
    }

    @Click(R.id.controlNext)
    void onNext() {
        mServiceViewModel.toNextMusic();
    }

    @SeekBarProgressChange(R.id.seekBarMusic)
    void onProgressChangeOnSeekBar(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            mServiceViewModel.seekTo(progress);
        }
    }

    @AfterViews
    void initAdapter() {
        TrackDataPreferences.init(getBaseActivity());
        // Get a new or existing ViewModel from the ViewModelProvider.
        mServiceViewModel = ViewModelProviders.of(getBaseActivity()).get(ServiceViewModel.class);
        mServiceViewModel.getMediaMetadata().observe(getBaseActivity(), mediaMetadataCompat -> {
            assert mediaMetadataCompat != null;
            TrackDataPreferences.addProperty(LONG_TRACK, MyMediaMetadata.getDuration(mediaMetadataCompat));
            setLength(MyMediaMetadata.getDuration(mediaMetadataCompat));
        });
        mServiceViewModel.getStatePlayback().observe(getBaseActivity(), playbackStateCompat -> {

        });
        setLength(TrackDataPreferences.getProperty(LONG_TRACK));



        // Get a new or existing ViewModel from the ViewModelProvider.
        mTrackViewModel = ViewModelProviders.of(getBaseActivity()).get(TrackViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mTrackViewModel.getAllTracks().observe(getBaseActivity(), new Observer<List<Track>>() {
            @Override
            public void onChanged(@Nullable final List<Track> words) {
                Log.e("getSize", String.valueOf(words.size()));
                tracks = words;
                if (!tracks.isEmpty()) {
//                    txtMaxTime.setText(getCorrectDuration(tracks.get(0).getDuration()));
                }
            }
        });
        subscription = Flowable.timer(1, TimeUnit.SECONDS)
                .repeat()
                .flatMapCompletable(i -> doStuffAsync())
                .subscribe();
    }


    private Completable doStuffAsync() {
        return Completable
                .fromRunnable(() -> {
                    txtNowTime.setText(Track.getCorrectDuration((int) mServiceViewModel.getPosition()));
                    mSeekBar.setProgress((int) mServiceViewModel.getPosition());
                })
                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> Log.e("getPosition","Stuff failed", e))
                .onErrorComplete();
    }

    @Override
    public void onDetach() {
        timer.cancel();
        subscription.dispose();
        super.onDetach();
    }

    private void setLength(long length) {
        txtMaxTime.setText(Track.getCorrectDuration(length));
        mSeekBar.setMax((int) length);
    }

}
