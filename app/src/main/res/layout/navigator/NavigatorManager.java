package com.example.yurii.mylikeproject.ui.navigator;

import com.example.yurii.mylikeproject.ui.navigator.core.Manager;
import com.example.yurii.mylikeproject.ui.navigator.core.NavigatorBaseManager;

import org.androidannotations.annotations.EBean;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public class NavigatorManager extends NavigatorBaseManager {

    @Override
    public Manager getMainManager(com.example.yurii.mylikeproject.ui.fragment.core.BaseFragment baseFragment) {

        return new ManagerMain(baseFragment,baseActivity);

    }

}
