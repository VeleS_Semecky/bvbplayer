package com.example.yurii.bvbplayer.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    @Expose
    private String mName;

    public User() {
    }

    public User(String mName) {
        this.mName = mName;
    }

    public String getName() {

        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
