package com.example.yurii.mylikeproject.ui.navigator.core;

import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public abstract class NavigatorBaseManager {

    @RootContext
    protected BaseActivity baseActivity;

    public abstract com.example.yurii.mylikeproject.ui.navigator.core.Manager getMainManager(com.example.yurii.mylikeproject.ui.fragment.core.BaseFragment baseFragment);

}
