package com.example.yurii.bvbplayer.ui.navigator;


import android.support.annotation.NonNull;

import com.example.yurii.bvbplayer.R;
import com.example.yurii.bvbplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bvbplayer.ui.fragment.FirstFragment_;
import com.example.yurii.bvbplayer.ui.fragment.core.BaseFragment;
import com.example.yurii.bvbplayer.ui.navigator.core.BaseManager;
import com.example.yurii.bvbplayer.ui.navigator.core.ResourceManager;
import com.example.yurii.bvbplayer.ui.navigator.core.ResourceNames;


import org.androidannotations.api.builder.FragmentBuilder;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends BaseManager {

    ManagerMain(BaseFragment baseFragment, BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case ResourceManager.FragmentId.FIRST_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, FirstFragment_.builder().build(), ResourceNames.FIRST_FRAGMENT).commit();
                break;
//            case ResourceManager.FragmentId.PLAYER_TRACK_FRAGMENT:
//                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, PlayerTrackFragment_.builder().build(), ResourceNames.PLAYER_TRACK_FRAGMENT).commit();
//                break;
//            case ResourceManager.FragmentId.PLAYER_CONTROL_FRAGMENT:
//                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerTwo, PlayerControlFragment_.builder().build(), ResourceNames.PLAYER_CONTROL_FRAGMENT).commit();
//                break;
//            case ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT:
//                replaceFragment(R.id.fragment_container, ListProductsFragment_.builder(),o[0].toString(),ResourceNames.LIST_PRODUCTS_FRAGMENT);
//                break;

        }
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, @NonNull String bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, int bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    @Override
    public void removeFragment() {
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
