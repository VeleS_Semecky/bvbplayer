package com.example.yurii.bvbplayer.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceManager {
    public static final class ActivityId{
        //1-20
        public final static int START_ACTIVITY = 1;


    }

    public static final class FragmentId{
        //100-500
        public final static int LIST_TRACK_FRAGMENT = 100;
        public final static int PLAYER_TRACK_FRAGMENT = 101;
        public final static int PLAYER_CONTROL_FRAGMENT = 102;
        public final static int FIRST_FRAGMENT = 102;


    }

    public static final class ToolbarId{
        //1000 - 1500
        public final static int SIMPLE = 1000;


    }
}
