package com.example.yurii.bvbplayer.bean.core;


import com.example.yurii.bvbplayer.api.deps.DaggerUserDeps;
import com.example.yurii.bvbplayer.api.deps.UserDeps;
import com.example.yurii.bvbplayer.api.model.User;
import com.example.yurii.bvbplayer.api.networking.NetworkUser;
import com.example.yurii.bvbplayer.api.networking.module.NetworkModule;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.io.File;

import javax.inject.Inject;

@EBean
public abstract class UserBean extends Bean<User> {
    private UserDeps userDeps;

    public UserDeps getUserDeps() {
        return userDeps;
    }

    @Inject
    public NetworkUser networkUser;


    @AfterInject
    public void init(){
        userDeps = DaggerUserDeps.builder().networkModule(

                new NetworkModule(
                        new File(
                                baseActivity.getCacheDir(),
                                "responses"),"http://httpbin.org/")).build();
    }
}
