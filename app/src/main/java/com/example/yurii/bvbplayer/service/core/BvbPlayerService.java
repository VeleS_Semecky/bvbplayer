package com.example.yurii.bvbplayer.service.core;

import android.arch.lifecycle.LifecycleService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;

import com.example.yurii.bvbplayer.service.BvbMediaSession;

public class BvbPlayerService extends LifecycleService {
    private BvbMediaSession myMediaSession;

    @Override
    public void onCreate() {
        super.onCreate();
        myMediaSession = new BvbMediaSession(this);
        getLifecycle().addObserver(myMediaSession);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaButtonReceiver.handleIntent(myMediaSession.getMediaSession(), intent);
        return super.onStartCommand(intent, flags, startId);
    }


    public class MyBinder extends Binder {
        public BvbPlayerService getService() {
            return BvbPlayerService.this;
        }

        public MediaSessionCompat.Token getMediaSessionToken() {
            return myMediaSession.getMediaSession().getSessionToken();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return new MyBinder();
    }

}
